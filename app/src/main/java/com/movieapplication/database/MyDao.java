package com.movieapplication.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.movieapplication.model.Item;

import java.util.List;
@Dao
public interface MyDao {
    @Insert
    public void AddData(Item item);
    @Query("SELECT * FROM  item")
    public List<Item> getMovieList();
    @Query("SELECT * FROM  item WHERE movie_id = :id")
    public boolean isExistMovieid(String id);
    @Query("SELECT * FROM  item ORDER BY vote_average DESC")
    public List<Item> sortByRating();
    @Query("SELECT * FROM  item ORDER BY release_date DESC")
    public List<Item> sortByDate();
}
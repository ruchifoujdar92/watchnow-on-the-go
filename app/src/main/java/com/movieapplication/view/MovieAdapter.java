package com.movieapplication.view;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.movieapplication.R;
import com.movieapplication.databinding.CustomMovieAdapterBinding;
import com.movieapplication.model.Item;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder>{

    private LayoutInflater mLayoutInflater;
    Context mContext;
    private List<Item> mMovieList;
    private String mAction;

    public MovieAdapter(Context mContext, List<Item> mMovieList,String action) {
        this.mContext = mContext;
        this.mMovieList = mMovieList;
        this.mAction=action;
    }
    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (mLayoutInflater == null) {
            mLayoutInflater = LayoutInflater.from(parent.getContext());
        }
        CustomMovieAdapterBinding binding = DataBindingUtil.inflate(mLayoutInflater,
                R.layout.custom_movie_adapter, parent, false);
        return new MovieViewHolder(binding,mContext,mAction);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        holder.binding.setItem(mMovieList.get(position));
    }

    @Override
    public int getItemCount() {
        if (mMovieList!=null){
            return mMovieList.size();
        }
        return 0;
    }

    static class MovieViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{

        private final CustomMovieAdapterBinding binding;
        private final Context context;
        private final String mAction;

        public MovieViewHolder(final CustomMovieAdapterBinding itemBinding,Context context,String action) {
            super(itemBinding.getRoot());
            itemBinding.relativeLayout.setOnClickListener(this);
            this.binding = itemBinding;
            this.context=context;
            this.mAction=action;
        }

        @Override
        public void onClick(View v) {
            Intent intent=new Intent(context,DetailActivity.class);
            intent.putExtra("position",getAdapterPosition()+"");
            intent.putExtra("action",mAction);
            context.startActivity(intent);

        }
    }
}

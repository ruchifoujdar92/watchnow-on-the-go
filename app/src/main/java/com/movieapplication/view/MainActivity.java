package com.movieapplication.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.movieapplication.network.NetworkConnectivity;
import com.movieapplication.viewmodel.MovieViewModel;
import com.movieapplication.R;
import com.movieapplication.databinding.ActivityMainBindingImpl;
import com.movieapplication.model.Item;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import static com.movieapplication.viewmodel.MovieViewModel.myDatabase;

public class MainActivity extends AppCompatActivity{

    private RecyclerView mRecyclerView_movieList;
    private MovieAdapter mMovieAdapter;
    private  MovieViewModel mMovieViewModel;
    private NetworkConnectivity mNetworkConnectivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ActivityMainBindingImpl activityMainBinding= DataBindingUtil.setContentView(this, R.layout.activity_main);

        mRecyclerView_movieList = findViewById(R.id.recyclerView_movieList);
        mRecyclerView_movieList.setHasFixedSize(true);
        mRecyclerView_movieList.setLayoutManager(new GridLayoutManager(this, 3));
        mMovieViewModel=new MovieViewModel(MainActivity.this);

        mNetworkConnectivity=new NetworkConnectivity(MainActivity.this);
        boolean isConnected = mNetworkConnectivity.isNetworkAvailable();
        if (isConnected==true)
        {
            activityMainBinding.refreshBtn.setVisibility(View.INVISIBLE);
            mMovieViewModel.getMovie(activityMainBinding).observe(MainActivity.this, new Observer<List<Item>>() {
                @Override
                public void onChanged(List<Item> items) {
                    mMovieAdapter = new MovieAdapter(MainActivity.this, items,"none");
                    mRecyclerView_movieList.setAdapter(mMovieAdapter);
                }
            });
        }
        else {
            Toast.makeText(MainActivity.this,"No internet connection",Toast.LENGTH_LONG).show();
            activityMainBinding.refreshBtn.setVisibility(View.VISIBLE);
        }
        activityMainBinding.refreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isConnected = mNetworkConnectivity.isNetworkAvailable();
                if (isConnected==true)
                {
                    activityMainBinding.refreshBtn.setVisibility(View.INVISIBLE);
                    mMovieViewModel.getMovie(activityMainBinding).observe(MainActivity.this, new Observer<List<Item>>() {
                        @Override
                        public void onChanged(List<Item> items) {
                            mMovieAdapter = new MovieAdapter(MainActivity.this, items,"none");
                            mRecyclerView_movieList.setAdapter(mMovieAdapter);
                        }
                    });
                }
                else {
                    Toast.makeText(MainActivity.this,"No internet connection",Toast.LENGTH_LONG).show();
                    activityMainBinding.refreshBtn.setVisibility(View.VISIBLE);
                }
            }
        });

        activityMainBinding.ratingTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMovieAdapter = new MovieAdapter(MainActivity.this, myDatabase.myDao().sortByRating(),"rating");
                mRecyclerView_movieList.setAdapter(mMovieAdapter);
            }
        });

        activityMainBinding.dateTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMovieAdapter = new MovieAdapter(MainActivity.this, myDatabase.myDao().sortByDate(),"date");
                mRecyclerView_movieList.setAdapter(mMovieAdapter);
            }
        });

    }
}

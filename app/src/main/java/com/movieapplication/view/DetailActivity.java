package com.movieapplication.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.movieapplication.R;
import com.movieapplication.databinding.ActivityDetailBindingImpl;
import com.movieapplication.model.Item;

import java.util.List;

import static com.movieapplication.viewmodel.MovieViewModel.myDatabase;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final ActivityDetailBindingImpl binding = DataBindingUtil.setContentView(this, R.layout.activity_detail);
        binding.playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DetailActivity.this,"Created for UI purpose",Toast.LENGTH_SHORT).show();
            }
        });

        Item item =new Item();
        Intent intent=getIntent();
        final String position = intent.getStringExtra("position");
        final String action = intent.getStringExtra("action");
        List<Item>movieList;
        switch (action){
            case "rating":
                movieList=myDatabase.myDao().sortByRating();
               setDataOnList(Integer.parseInt(position),movieList,binding,item);
                break;
            case "date":
                movieList=myDatabase.myDao().sortByDate();
               setDataOnList(Integer.parseInt(position),movieList,binding,item);
                break;
            default:
                movieList=myDatabase.myDao().getMovieList();
                setDataOnList(Integer.parseInt(position),movieList,binding,item);
                break;
        }
    }
    public void setDataOnList(int position,List<Item> movieList,ActivityDetailBindingImpl binding,Item item){

        System.out.println(movieList.get(position).getBackdropPath());
        item.setBackdropPath(movieList.get(position).getBackdropPath());
        item.setTitle(movieList.get(position).getTitle());
        getSupportActionBar().setTitle(movieList.get(position).getTitle());
        item.setReleaseDate(movieList.get(position).getReleaseDate());
        item.setVoteAverage(movieList.get(position).getVoteAverage());
        String value =movieList.get(position).getPopularity();
        String[] splitStr = value.split("\\.");
        item.setPopularity(splitStr[0]+"% Popular among others");
        item.setOverview(movieList.get(position).getOverview());
        binding.setItem(item);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
}


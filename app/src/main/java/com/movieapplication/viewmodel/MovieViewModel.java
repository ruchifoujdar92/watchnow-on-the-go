package com.movieapplication.viewmodel;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.room.Room;

import com.movieapplication.api.Api;
import com.movieapplication.database.MyDatabase;
import com.movieapplication.databinding.ActivityMainBindingImpl;
import com.movieapplication.model.Item;
import com.movieapplication.model.Movie;
import com.movieapplication.network.NetworkConnectivity;
import com.movieapplication.view.MainActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MovieViewModel extends ViewModel {

    private static Context mContext;
    private MutableLiveData<List<Item>> mMovieList;
    public static MyDatabase myDatabase;
    private ActivityMainBindingImpl mActivityMainBinding;

    public MovieViewModel(Context context) {
        this.mContext= context;
        myDatabase = Room.databaseBuilder(context, MyDatabase.class, "myinfo")
                .allowMainThreadQueries().build();
    }
    public LiveData<List<Item>> getMovie(ActivityMainBindingImpl activityMainBinding) {
        this.mActivityMainBinding=activityMainBinding;
        //if the list is null
        if (mMovieList == null) {
            mMovieList = new MutableLiveData<List<Item>>();
            loadMovie();
        }
        return mMovieList;
    }
    private void loadMovie() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api api = retrofit.create(Api.class);
        Call<Movie> call = api.getMovieList(1, Api.API_KEY);
        call.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                //finally we are setting the list to our MutableLiveData
                if (response.code() == 200) {
                    Movie movie = response.body();
                    List<Item> items = movie.getItems();
                    Item item = new Item();
                    for (int i = 0; i < items.size(); i++) {
                        item.setPosterPath(items.get(i).getPosterPath());
                        item.setBackdropPath(items.get(i).getBackdropPath());
                        item.setTitle(items.get(i).getTitle());
                        item.setOverview(items.get(i).getOverview());
                        item.setVoteAverage(items.get(i).getVoteAverage());
                        item.setReleaseDate(items.get(i).getReleaseDate());
                        item.setPopularity(items.get(i).getPopularity());
                        item.setId(items.get(i).getId());
                        boolean isExits =myDatabase.myDao().isExistMovieid(items.get(i).getId());
                        System.out.println(isExits);
                        if (myDatabase.myDao().isExistMovieid(items.get(i).getId())==true)
                        {
                            //do not execute anything
                        }
                        else {
                            myDatabase.myDao().AddData(item);
                        }
                    }
                    getdata();
                    mActivityMainBinding.refreshBtn.setVisibility(View.INVISIBLE);

                } else if (response.code() == 400) {
                }
            }
            @Override
            public void onFailure(Call<Movie> call, Throwable t) {
            }
        });
    }
    public void getdata() {
        List<Item> itemArrayList;
        itemArrayList = myDatabase.myDao().getMovieList();
        mMovieList.setValue(itemArrayList);
    }

}

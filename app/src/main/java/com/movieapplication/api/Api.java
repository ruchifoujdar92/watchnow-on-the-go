package com.movieapplication.api;

import com.movieapplication.model.Movie;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api {
    String BASE_URL = "https://api.themoviedb.org/3/";
    String API_KEY = "2eed2a2b7708f8f653796d42631f8514";
    String BASE_URL_IMAGE ="https://image.tmdb.org/t/p/original/";

    @GET("list/{list_id}?")
    Call<Movie> getMovieList(@Path("list_id") int list_id, @Query("api_key") String api_key);
}

package com.movieapplication.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Movie{

    @SerializedName("items")
    @Expose
    private List<Item> items = null;
    public List<Item> getItems() {
        return items;
    }

}



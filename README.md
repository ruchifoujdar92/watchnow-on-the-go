# Android WatchNow-On the go application

WatchNow-On the go which lists movies which a user can watch using an internet connectivity.
Using https://www.themoviedb.org/ API to fetch list of movies.
Compatible with android 14 and higher
Followed best UI practises.

a) Architectural Design pattern used: MVVM(Model-View-ViewModel)

b) For icons used: Flaticon(free icons)

c) Application has two different screens:
1) MainActivity which lists movies
2) DetailActivity which list movie details when user clicks on any movie from MainActivity
It also consists:
Database: Using Room database to store data in local database.
Network Connectivity: to check if intenet connection is availabale or not


